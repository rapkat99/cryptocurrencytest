//
//  Service.swift
//  CryptoCurrencyTestApp
//
//  Created by Baudunov Rapkat on 1/10/21.
//

import Foundation
import UIKit
import Alamofire
import Starscream

struct Constants {
    static let apiKey = "5AA53CEE-B0CD-4CF3-AFB4-E46A397CBE0A"
}

class Service {
    let baseUrl = "https://rest.coinapi.io/"
    let socketUrl = "wss://ws.coinapi.io/v1/"
    let apiKey = Constants.apiKey
    
   
    static let instance = Service()

    func headerRequest() -> HTTPHeaders{
        let headers: HTTPHeaders = [
            "X-CoinAPI-Key" : apiKey
        ]
        return headers
    }
    
    
    func handleResponse<T: Decodable>(model: T.Type, response: DataResponse<T, AFError>, completion: @escaping (Result<T, Error>) -> Void) {
        switch response.result {
        case .success(let t):
            completion(.success(t))
        case .failure(let err):
            print(err)
        }
    }
    
    func getAllSymbols(completion: @escaping (Result<[SymbolsModel], Error>) -> Void) {
        let url = baseUrl.appending("v1/symbols/BINANCE")
       
        AF.request(url,
                   method: .get,
                   headers: headerRequest())
            .validate()
            .responseDecodable(of: [SymbolsModel].self) { (response) in
                self.handleResponse(model: [SymbolsModel].self,
                                    response: response,
                                    completion: completion)
            }
    }
    
    func getHistoryLatestData(symbol_id: String, completion: @escaping (Result<[HistoryLatestModel], Error>) -> Void) {
        let url = baseUrl.appending("v1/ohlcv/\(symbol_id)/latest")
        let parameters: [String: String] = ["period_id": "10Day",
                                            "limit": "100" ]
        AF.request(url,
                   method: .get,
                   parameters: parameters,
                   headers: headerRequest())
            .validate()
            .responseDecodable(of: [HistoryLatestModel].self) { (response) in
                self.handleResponse(model: [HistoryLatestModel].self,
                                    response: response,
                                    completion: completion)
            }
    }
    
    func getSymbolsBySymbolId(symbol_id: String, exchange_id: String, completion: @escaping (Result<[SymbolsModel], Error>) -> Void) {
        let url = baseUrl.appending("v1/symbols")
        let parameters: [String: String] = ["filter_symbol_id": symbol_id,
                                            "filter_exchange_id": exchange_id ]
        AF.request(url,
                   method: .get,
                   parameters: parameters,
                   headers: headerRequest())
            .validate()
            .responseDecodable(of: [SymbolsModel].self) { (response) in
                self.handleResponse(model: [SymbolsModel].self,
                                    response: response,
                                    completion: completion)
            }
    }
}
