//
//  HistoryLatestModel.swift
//  CryptoCurrencyTestApp
//
//  Created by Baudunov Rapkat on 5/10/21.
//

import Foundation

struct HistoryLatestModel: Decodable {
    var time_period_start: String?
    var time_period_end: String?
    var time_open: String?
    var time_close: String?
    var price_close: Double?
}
