//
//  SymbolsModel.swift
//  CryptoCurrencyTestApp
//
//  Created by Baudunov Rapkat on 5/10/21.
//

struct SymbolsModel: Decodable {
    var symbol_id: String?
    var exchange_id: String?
    var symbol_type: String?
    var asset_id_base: String?
    var asset_id_quote: String?
    var data_start: String?
    var data_end: String?
    var data_trade_start: String?
    var data_trade_end: String?
    var price: Double?
}
