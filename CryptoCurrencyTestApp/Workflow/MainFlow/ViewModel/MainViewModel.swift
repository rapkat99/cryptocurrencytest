//
//  MainViewModel.swift
//  CryptoCurrencyTestApp
//
//  Created by Baudunov Rapkat on 2/10/21.
//

import Foundation

class MainViewModel {
    private var service = Service.instance
    
    func getHistoryLatestData(symbol_id: String, completion: @escaping (Result<[HistoryLatestModel], Error>) -> Void) {
        service.getHistoryLatestData(symbol_id: symbol_id, completion: completion)
    }
    
    func getSymbolsBySymbolId(symbol_id: String, exchange_id: String, completion: @escaping (Result<[SymbolsModel], Error>) -> Void) {
        service.getSymbolsBySymbolId(symbol_id: symbol_id, exchange_id: exchange_id, completion: completion)
    }
}
