//
//  ChooseCryptoViewModel.swift
//  CryptoCurrencyTestApp
//
//  Created by Baudunov Rapkat on 4/10/21.
//

import Foundation

class ChooseCryptoViewModel {
    private var service = Service.instance

    func getAllSymbols(completion: @escaping (Result<[SymbolsModel], Error>) -> Void) {
        service.getAllSymbols(completion: completion)
    }
}
